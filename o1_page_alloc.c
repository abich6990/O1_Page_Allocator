

#include "stdio.h"
#include "stdint.h"
#include <stdlib.h>

#define PAGE_SHIFT 12
#define PAGE_SIZE (1 << PAGE_SHIFT)
#define MEMORY_SIZE (1 << 17)
#define PAGES (MEMORY_SIZE / PAGE_SIZE)

/** Details:
 * MEMORY: must be page aligned to simulate the page allocation correctly (but is only here
 * for others who want to play around with "real" memory, while trying the page allocation).
 * This "memory" is of course not needed when using this page allocator in an operating system!
 *
 * LIN_VIRT: is only page aligned for beauty reasons. It is used to have a linear view of the
 * allocated pages (just like in virtual memory). To be more accurate, it is the mapping from
 * virtual addresses (divided into pages and limited to the size of the physical
 * memory) to physical pages. With this linear view it is possible
 * to allocate pages with O(1), i.e. constant time. And not just is the time constant, it is also
 * quite short (only a handful of instructions). Allocation is easy because this view is always
 * sorted into used (index lower than NEXT_FREE_LV_IDX) and unused pages.
 *
 * LIN_PHYS: is as well only aligned for beauty reasons. It is as well a linear view but this
 * time from the physical memory perspective. Each physical page knows which virtual page
 * it is. This makes it possible to easily change the position of two virtual pages to
 * sort them as described above.
 */
uint8_t memory[MEMORY_SIZE] __attribute__((aligned(PAGE_SIZE)));
size_t virt_pages[PAGES] __attribute__((aligned(PAGE_SIZE)));
size_t phys_pages[PAGES] __attribute__((aligned(PAGE_SIZE)));
size_t next_free_virt_idx = 0; // next free virt_pages, also number of allocated/used virt_pagess 

void init(){
  for(size_t idx = 0; idx < PAGES; idx++){
    virt_pages[idx] = phys_pages[idx] = idx;
  }
}

size_t page_alloc(){
  if(next_free_virt_idx >= PAGES){
    printf("ALLOC:\tvirtual idx=%zu\tERR=Out-Of-Memory\n", next_free_virt_idx);
    return -1;
  }
  
  size_t free_lv_idx = next_free_virt_idx++;
  size_t phys_idx = virt_pages[free_lv_idx]; // page index to write into memory, i.e. the static variable memory (multiply by PAGE_SIZE to get physical address when mapping for paging)

  printf("ALLOC:\tphysical idx=%zu, with virtual idx=%zu\n", phys_idx, free_lv_idx);
  // here you would normally map the physical page address to your memory (page directory -> page table -> page table entry)

  return phys_idx;
}

size_t page_free(size_t to_free_phys_idx){
  if(to_free_phys_idx >= PAGES){
    printf("FREE:\tphysical idx=%zu\tERR=Out-Of-Range\n", to_free_phys_idx);
    return -1;
  }
  
  size_t virt_idx = phys_pages[to_free_phys_idx];
  if(virt_idx >= next_free_virt_idx){
    printf("FREE:\tphysical idx=%zu, with virtual idx=%zu;\tfree virtual idx=%zu\tERR=Repeated-Free-Call\n", to_free_phys_idx, virt_idx, next_free_virt_idx);
    return -1;
  }
  
  size_t to_free_virt_idx = --next_free_virt_idx;
  /* rewire the virtual and physical connection of the pages, i.e. swap virt0 with
   * virt1 and phys0 with phys1. Swap is done between one used and
   * unused (to_free_*) page virt/phys pair
   */
  size_t used_virt_idx = phys_pages[to_free_phys_idx];
  virt_pages[used_virt_idx] = virt_pages[to_free_virt_idx];
  phys_pages[to_free_phys_idx] = to_free_virt_idx;
  phys_pages[virt_pages[to_free_virt_idx]] = used_virt_idx;
  virt_pages[to_free_virt_idx] = to_free_phys_idx;

  printf("FREE:\tphysical idx=%zu, with virtual idx=%zu;\tassigned virtual idx=%zu\n", to_free_phys_idx, virt_idx, to_free_virt_idx);
  return next_free_virt_idx;
}

int main(int argc, char **argv){
  printf("PS=%i, MS=%i, P=%i\n", PAGE_SIZE, MEMORY_SIZE, PAGES);

  init();

  for(size_t k = 0; k < 6; k++){
    for(size_t i = 0; i < 40; i++)
      page_alloc();

    for(size_t i = 0; i < 20; i++)
      page_free(rand() % PAGES);
  }
  return 0;
}



